#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

void intro()
{

    printf("\n\n");
    printf("\t***********************************************************************************\n");
    printf("\t*                                                                                 *\n");
    printf("\t*                                                                                 *\n");
    printf("\t*                    BEM VINDO AO PROGRAMA DE CALCULO DE PRESSAO                  *\n");
    printf("\t*                                                                                 *\n");
    printf("\t*                                                                                 *\n");
    printf("\t***********************************************************************************\n");
    printf("\n\n\n\n\n\n\n");

    system("pause");
    system("cls");
}


void calculo(double v, double T, int x, int y, double M[x][y])
{
    int i=0, j=0;
    int g;
    double A, B, P;

    FILE *arq=NULL;
    arq=fopen("C:\\Users\\User\\Desktop\\APS\\Parametros.txt","r");

    if(arq==NULL)
    {
        printf("Problema na abertura do arquivo \n");
        system("pause");
        exit(1);
    }

    for(i=0; i<x; i++)
    {
        for(j=0; j<y-1; j++)
        {
            if(fscanf(arq,"%lf\t", &M[i][j])==EOF)
            {
                printf("\nErro ao ler os parametros\n");
                system("pause");
                exit(1);
            }
        }

        if(fscanf(arq,"%lf\n", &M[i][j])==EOF)
        {
            printf("\nErro ao ler os parametros\n");
            system("pause");
            exit(1);
        }
    }

    fclose(arq);


    printf("\nESCOLHA O GAS PARA O CALCULO DE PRESSAO !!\n\n");
    printf("\t1-Ar\n\t2-Argonio(AR)\n\t3-Dioxido de Carbono(CO2)\n\t4-Helio(He)\n\t5-Hidrogenio(H2)\n\t6-Nitrogenio(N2)\n\t7-Oxigenio(O2)\n");
    printf("\n\nDigite o numero do gas desejado: ");
    scanf("%d", &g);

    while(g>7 || g<1)
    {
        printf("\n\n\nOpcao invalida!!\n\n\n\n");
        system("pause");
        system("cls");
        printf("\nESCOLHA O GAS PARA O CALCULO DE PRESSAO !!\n\n");
        printf("\t1-Ar\n\t2-Argonio(AR)\n\t3-Dioxido de Carbono(CO2)\n\t4-Helio(He)\n\t5-Hidrogenio(H2)\n\t6-Nitrogenio(N2)\n\t7-Oxigenio(O2)\n");
        printf("\n\nDigite outro valor do gas: ");
        scanf("%d", &g);

    }


    A = (M[g-1][0])*(1-(M[g-1][1]/v));

    B = (M[g-1][2])*(1-(M[g-1][3]/v));

    P = ((8.314*T)/(v*v))*((1)-(M[g-1][4]/(v*T*T*T)))*(v+B)-(A/(v*v));

    printf("\n\n>>> A pressao eh: %lf kPa\n\n\n\n", P);

    system("pause");
    system("cls");

}

int main()
{
    intro();
    double Matriz[7][5];
    double v, T;
    int OP;    while (OP!=4)
    {
        printf("Digite uma das opcoes\n\t1-Calcular Pressao\n\t2-Apresentar Equacionamento\n\t3-Valores Tabelados\n\t4-Sair\n");
        printf("Numero: ");
        scanf("%d", &OP);

        switch(OP)
        {
        case 1:
            system("cls");
            printf("\nDigite o VOLUME MOLAR do gas em m^3/kmol: ");
            scanf("%lf", &v);
            setbuf(stdin, NULL);

            printf("\nDigite a TEMPERATURA do gas em K: ");

            scanf("%lf", &T);
            setbuf(stdin, NULL);
            printf("\n");
            system("Pause");
            system("cls");

            calculo(v, T, 7, 5, &Matriz[0][0]);
            break;
        case 2:
            system("cls");
            printf("\nEquacoes Utilizadas!\n\n\n");
            printf("\tP = [(R*T)/(v^2)]*[1-c/(v*T^3)]*(v+B) - A/(v^2)\n\n");
            printf("\n\nSendo que: \n\n");
            printf("\tA = Ao*(1-a/v)\t  e\t B = Bo*(1-b/v)\n\n \n\n\t>> Valores de Ao, a, Bo, b e c sao tabelados! <<\n\n\n\n\n\n");
            system("pause");
            system("cls");
            break;
        case 3:
            system("cls");
            printf("\n\tGas\t\t\tAo\t\ta\t\tBo\t\tb\t\tc\n");
            printf("\n\tAr\t\t\t131.8441\t0.019310\t0.046110\t-0.001101\t43400\n");
            printf("\n\tArgonio(AR)\t\t130.7802\t0.023280\t0.039310\t0.000000\t59900\n");
            printf("\n\tDioxido de Carbono(CO2)\t507.2836\t0.071320\t0.104760\t0.072350\t660000\n");
            printf("\n\tHelio(He)\t\t2.188600\t0.059840\t0.014000\t0.000000\t40\n");
            printf("\n\tHidrogenio(H2)\t\t20.01170\t-0.00506\t0.020960\t-0.043590\t504\n");
            printf("\n\tNitrogenio(N2)\t\t136.2315\t0.026170\t0.050460\t-0.006910\t42000\n");
            printf("\n\tOxigenio(O2)\t\t151.0857\t0.025620\t0.046240\t0.004208\t48000\n\n");
            printf("\n\n\n\n");
            system("pause");
            system("cls");
            break;
        case 4:
            system("cls");
            printf("\n\n\tOBRIGADO POR UTILIZAR O PROGRAMA !! ");
            printf("\n\n\n\n\n");
            printf("\tDesenvolvedores: Camila dos Santos\n ");
            printf("\t                 Murilo H Trombini\n\n");
            printf("\n\n\n\n");
            printf("\t>> Universidade Tecnologica Federal do Parana - Campus Apucarana <<\n\n\n");
            system("pause");
            system("cls");
            break;

        default:
            system("cls");
            printf("\nOpcao Invalida !!!!\n\n\n\n");
            system("pause");
            system("cls");
            break;

        }
    }

    return 0;
}
